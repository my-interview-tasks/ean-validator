package dlesiak.interview.example;

import dlesiak.interview.ean.Addon;
import dlesiak.interview.ean.EanCode;
import dlesiak.interview.ean.EanType;
import dlesiak.interview.ean.Error;

import java.util.Optional;

public class Example {

    public static void main(String[] a){
        EanCode eanCode = new EanCode("692070270772112", EanType.THIRTEEN);
        boolean hasError = eanCode.hasError();
        String cleanedEan = eanCode.cleanedEan();
        String rawEan = eanCode.rawEan();
        Optional<Addon> addon = eanCode.addon();
        Optional<Error> error = eanCode.error();

        EanCode eanCode2 = new EanCode("075678164125", EanType.THIRTEEN);
    }
}
