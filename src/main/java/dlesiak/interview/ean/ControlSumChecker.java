package dlesiak.interview.ean;

class ControlSumChecker {

    static boolean isSumControlInvalid(String eanCode) {
        int total = 0;

        char[] chars = getCodeWithoutSumControl(eanCode);

        int index = 1;

        for (int i = chars.length - 1; i >= 0; i--) {
            if (index % 2 == 0) {
                total += Character.getNumericValue(chars[i]);
            } else {
                total += Character.getNumericValue(chars[i]) * 3;
            }
            index++;
        }

        int controlNumber = 10 - (total % 10);
        return getSumControl(eanCode) != controlNumber;
    }

    private static char[] getCodeWithoutSumControl(String eanCode) {
        return eanCode
                .substring(0, eanCode.length() - 1)
                .toCharArray();
    }

    private static int getSumControl(String eanCode) {
        return Character.getNumericValue(eanCode.charAt(eanCode.length() - 1));
    }
}
