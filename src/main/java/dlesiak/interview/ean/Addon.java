package dlesiak.interview.ean;

import java.util.Objects;

public final class Addon {
    static final int[] ADDONS_LENGTH = {5, 2}; //todo investigate has to be from highest to lower

    private final String value;

    Addon(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Addon addon = (Addon) o;
        return Objects.equals(value, addon.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }
}
