package dlesiak.interview.ean;

import io.vavr.API;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

import static dlesiak.interview.ean.ControlSumChecker.isSumControlInvalid;
import static dlesiak.interview.ean.Error.*;
import static io.vavr.API.*;

interface Validators {

    BiFunction<Predicate<CleanedEan>, Error, Function<CleanedEan, CleanedEan>> omitWhileErrorAlready = (test, error) ->
            ean -> Match(ean).of(
                    Case(API.$(CleanedEan::hasError), ean),
                    Case($(test), ean.error(error)),
                    Case($(), ean));

    Function<CleanedEan, CleanedEan> controlSumCheck = omitWhileErrorAlready.apply(
            ean -> isSumControlInvalid(ean.getCleanedCode()), INVALID_CONTROL_SUM);

    Function<CleanedEan, CleanedEan> containsNumbersOnly = omitWhileErrorAlready.apply(
            ean -> !ean.getCleanedCode().matches("^[0-9]*$"), CONTAINS_OTHER_CHARS);

    Function<CleanedEan, CleanedEan> isNotBlank = omitWhileErrorAlready.apply(
            ean -> ean.getCleanedCode().trim().length() == 0, INVALID_LENGTH);

    Function<CleanedEan, CleanedEan> validLength = omitWhileErrorAlready.apply(
            ean -> !ean.type().valid(ean.getCleanedCode()), INVALID_LENGTH);

}
