package dlesiak.interview.ean;

import java.util.function.Function;

interface MissingZeroAdd {

    Function<CleanedEan, CleanedEan> addMissingZero = ean -> ean.hasError() ? ean : addZeroIfMissing(ean);

    static CleanedEan addZeroIfMissing(CleanedEan ean) {
        if (isMissingZero(ean)) {
            String value = ean.getCleanedCode();
            return ean.code("0" + value);
        }
        return ean;
    }

    static boolean isMissingZero(CleanedEan cleanedEan) {
        String eanCode = cleanedEan.getCleanedCode();
        EanType type = cleanedEan.type();
        final int eanLengthWithMissingZero = type.length() - 1;

        if (eanCode.charAt(0) == '0') {
            if (eanCode.length() == eanLengthWithMissingZero) {
                return true;
            } else {
                for (int addon : Addon.ADDONS_LENGTH)
                    if (eanLengthWithMissingZero + addon == eanCode.length())
                        return true;
            }
        }
        return false;
    }

}
