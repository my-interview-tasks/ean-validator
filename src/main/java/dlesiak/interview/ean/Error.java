package dlesiak.interview.ean;

public enum Error {
    CONTAINS_OTHER_CHARS,
    INVALID_LENGTH,
    INVALID_CONTROL_SUM,
}
