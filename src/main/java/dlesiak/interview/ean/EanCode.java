package dlesiak.interview.ean;

import java.util.Objects;
import java.util.Optional;

import static dlesiak.interview.ean.AddonRemover.*;
import static dlesiak.interview.ean.MissingZeroAdd.*;
import static dlesiak.interview.ean.Validators.*;

public final class EanCode {
    private final CleanedEan cleanedEan;

    public EanCode(String rawCode, EanType type) {
        cleanedEan = clean(rawCode, type);
    }

    private CleanedEan clean(String rawCode, EanType type) {
        return Optional.ofNullable(rawCode)
                .map(code -> new CleanedEan(code, type))
                .map(isNotBlank
                        .andThen(containsNumbersOnly)
                        .andThen(addMissingZero)
                        .andThen(removeAddon)
                        .andThen(validLength)
                        .andThen(controlSumCheck)
                ).orElseThrow(NullPointerException::new);
    }

    public boolean hasError() {
        return cleanedEan.hasError();
    }

    public Optional<Error> error() {
        return cleanedEan.getError();
    }

    public Optional<Addon> addon() {
        return cleanedEan.getAddon();
    }

    public String cleanedEan() {
        return cleanedEan.getCleanedCode();
    }

    public String rawEan() {
        return cleanedEan.getRawCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EanCode eanCode = (EanCode) o;
        return Objects.equals(rawEan(), eanCode.rawEan());
    }

    @Override
    public int hashCode() {
        return Objects.hash(rawEan());
    }

}
