package dlesiak.interview.ean;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;
import java.util.Optional;

final class CleanedEan {
    @NotNull
    private final String rawCode;
    @NotNull
    private final String code;
    @Nullable
    private final Addon addon;
    @Nullable
    private final Error error;
    @NotNull
    private final EanType eanType;

    private CleanedEan(String rawCode, String code, EanType eanType, Addon addon, Error error) {
        Objects.requireNonNull(rawCode);
        Objects.requireNonNull(eanType);
        this.rawCode = rawCode;
        this.code = code;
        this.eanType = eanType;
        this.addon = addon;
        this.error = error;
    }

    CleanedEan(String rawCode, EanType eanType) {
        this(rawCode, rawCode, eanType, null, null);
    }

    CleanedEan addon(Addon addon) {
        return new CleanedEan(rawCode, code, eanType, addon, error);
    }

    CleanedEan error(Error error) {
        return new CleanedEan(rawCode, code, eanType, addon, error);
    }

    CleanedEan code(String code) {
        return new CleanedEan(rawCode, code, eanType, addon, error);
    }

    String getCleanedCode() {
        if (hasError()) {
            throw new IllegalStateException("Could not clean code. Error!");
        }
        return code;
    }

    Optional<Addon> getAddon() {
        return Optional.ofNullable(addon);
    }

    Optional<Error> getError() {
        return Optional.ofNullable(error);
    }

    boolean hasError() {
        return getError().isPresent();
    }

    EanType type() {
        return eanType;
    }

    String getRawCode() {
        return rawCode;
    }
}
