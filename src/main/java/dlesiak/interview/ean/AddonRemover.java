package dlesiak.interview.ean;

import java.util.function.Function;

import static dlesiak.interview.ean.Addon.ADDONS_LENGTH;

interface AddonRemover {

    Function<CleanedEan, CleanedEan> removeAddon = ean -> ean.hasError() ? ean : removeAddons(ean);

    static CleanedEan removeAddons(CleanedEan cleanedEan) {
        String eanCode = cleanedEan.getCleanedCode();
        EanType type = cleanedEan.type();
        Addon addon = null;
        if (eanCode.length() > type.length) {
            for (int addonLength : ADDONS_LENGTH) {
                if (eanCode.length() == type.length + addonLength) {
                    String addonValue = eanCode.substring(eanCode.length() - addonLength);
                    addon = new Addon(addonValue);
                    eanCode = eanCode.substring(0, eanCode.length() - addonLength);
                    break;
                }
            }
        }
        return cleanedEan.addon(addon).code(eanCode);
    }


}
