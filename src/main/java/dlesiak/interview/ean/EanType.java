package dlesiak.interview.ean;

public enum EanType {
    EIGHT(8), THIRTEEN(13);

    int length;

    EanType(int length) {
        this.length = length;
    }

    int length() {
        return length;
    }

    boolean valid(String code) {
        if (code == null)
            return false;
        return length == code.length();
    }

}
