package dlesiak.interview.ean;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class EanCodeTest {

    @Test
    void returnErrorIfCodeIsNull() {
        assertThrows(NullPointerException.class, () -> new EanCode(null, EanType.EIGHT));
    }

    @Test
    void returnErrorIfCodeTypeIsNull() {
        assertThrows(NullPointerException.class, () -> new EanCode("code", null));
    }

    @ParameterizedTest
    @CsvSource(value = {
            "6920702707721, THIRTEEN, 6920702707721",
            "0075678164125, THIRTEEN, 0075678164125",
            "53452351, EIGHT, 53452351",
            "00423427, EIGHT, 00423427",
    })
    void shouldReturnValidCodeWhenProcessingValidCode(String code, String type, String validCode) {
        EanType eanType = EanType.valueOf(type);
        EanCode eanCode = new EanCode(code, eanType);

        codeIsValid(eanCode);
        codeHasNoError(eanCode);
        assertEquals(validCode, eanCode.cleanedEan());
        assertFalse(eanCode.addon().isPresent());
        assertEquals(code, eanCode.rawEan());
    }

    @ParameterizedTest
    @CsvSource(value = {
            //code with addon 2
            "692070270772112, THIRTEEN, 6920702707721, 12",
            //code with addon 5
            "692070270772112212, THIRTEEN, 6920702707721, 12212",
            //code with addon 2 and missing zero
            "07567816412522, THIRTEEN, 0075678164125, 22",
            //code with addon 5 and missing zero
            "07567816412554321, THIRTEEN, 0075678164125, 54321",

            //code with addon 2
            "3245235822, EIGHT, 32452358, 22",
            //code with addon 5
            "3245235855555, EIGHT, 32452358, 55555",
            //code with addon 2 and missing zero
            "023423822, EIGHT, 00234238, 22",
            //code with addon 5 and missing zero
            "023423843215, EIGHT, 00234238, 43215",
    })
    void returnValidCodeWithoutAddons(String code, String type, String validCode, String addon) {
        EanType eanType = EanType.valueOf(type);
        EanCode eanCode = new EanCode(code, eanType);

        codeIsValid(eanCode);
        codeHasNoError(eanCode);
        assertEquals(validCode, eanCode.cleanedEan());
        assertEquals(code, eanCode.rawEan());
        //addon is present
        assertTrue(eanCode.addon().isPresent());
        assertEquals(new Addon(addon), eanCode.addon().get());
    }


    @ParameterizedTest
    @CsvSource(value = {
            //with missing zero
            "075678164125, THIRTEEN, 0075678164125",
            //with missing zero and addon 2
            "07567816412522, THIRTEEN, 0075678164125",
            //with missing zero and addon 5
            "07567816412533441, THIRTEEN, 0075678164125",

            //with missing zero
            "0234238, EIGHT, 00234238",
            //with missing zero and addon 2
            "023423812, EIGHT, 00234238",
            //with missing zero and addon 5
            "023423812345, EIGHT, 00234238",

    })
    void returnValidCodeWithoutMissingZero(String code, String type, String validCode) {
        EanType eanType = EanType.valueOf(type);
        EanCode eanCode = new EanCode(code, eanType);

        codeIsValid(eanCode);
        codeHasNoError(eanCode);
        assertEquals(validCode, eanCode.cleanedEan());
        assertEquals(code, eanCode.rawEan());
    }

    private void codeHasNoError(EanCode eanCode) {
        assertFalse(eanCode.error().isPresent());
    }


    private void codeIsValid(EanCode eanCode) {
        assertFalse(eanCode.hasError());
    }

    @ParameterizedTest
    @CsvSource(value = {
            //normal code
            "00234233, EIGHT",
            // with addon 2
            "0023423312, EIGHT",
            // with addon 5
            "0023423354321, EIGHT",
            //missing zero
            "0234233, EIGHT",
            //missing zero addon 2
            "023423311, EIGHT",
            //missing zero addon 5
            "023423354321, EIGHT",

            //normal code
            "075678164124, THIRTEEN",
            // with addon 2
            "692070270772312, THIRTEEN",
            // with addon 5
            "692070270772412345, THIRTEEN",
            //missing zero
            "075678164128, THIRTEEN",
            //missing zero and addon 2
            "07567816412833, THIRTEEN",
            //missing zero and addon 5
            "07567816412833445, THIRTEEN",
    })
    void shouldReturnInvalidCodeWhenControlSumIsInvalid(String code, String type) {
        //given
        EanType eanType = EanType.valueOf(type);

        //when
        EanCode eanCode = new EanCode(code, eanType);

        //then
        codeIsNotValid(eanCode);
        codeHasError(eanCode, Error.INVALID_CONTROL_SUM);
        cannotObtainCleanedCode(eanCode);
        assertEquals(code, eanCode.rawEan());
    }


    @ParameterizedTest
    @CsvSource(value = {
            "002g4d233, EIGHT",
            "002g4d2.3, EIGHT",
            "002g4d2/3 ,EIGHT",

            "07567d1f4124, THIRTEEN",
            "075678164'4,THIRTEEN",
            "075678164.24,THIRTEEN",
    })
    void shouldReturnInvalidCodeWhenCodeContainsNonNumericChars(String code, String type) {
        //given
        EanType eanType = EanType.valueOf(type);

        //when
        EanCode eanCode = new EanCode(code, eanType);

        //then
        codeIsNotValid(eanCode);
        codeHasError(eanCode, Error.CONTAINS_OTHER_CHARS);
        cannotObtainCleanedCode(eanCode);
        assertEquals(code, eanCode.rawEan());
    }

    @ParameterizedTest
    @CsvSource(value = {
            "07564124, THIRTEEN",
            "075456346346644324324124, THIRTEEN",
            "1, THIRTEEN",

            "02311, EIGHT",
            "0223534542345311, EIGHT",
            "1, EIGHT",
    })
    void shouldReturnInvalidCodeWhenCodeLengthIsInvalid(String code, String type) {
        //given
        EanType eanType = EanType.valueOf(type);

        //when
        EanCode eanCode = new EanCode(code, eanType);

        //then
        codeIsNotValid(eanCode);
        codeHasError(eanCode, Error.INVALID_LENGTH);
        cannotObtainCleanedCode(eanCode);
        assertEquals(code, eanCode.rawEan());
    }


    @Test
    void shouldReturnInvalidCodeWhenCodeIsBlank() {
        //when
        EanCode eanCode = new EanCode("", EanType.EIGHT);

        //then
        codeIsNotValid(eanCode);
        codeHasError(eanCode, Error.INVALID_LENGTH);
        cannotObtainCleanedCode(eanCode);
        assertEquals("", eanCode.rawEan());
    }

    private void cannotObtainCleanedCode(EanCode eanCode) {
        assertThrows(IllegalStateException.class, eanCode::cleanedEan);
    }

    private void codeIsNotValid(EanCode eanCode) {
        assertTrue(eanCode.hasError());
    }


    private void codeHasError(EanCode eanCode, Error error) {
        assertTrue(eanCode.error().isPresent());
        assertEquals(error, eanCode.error().get());
    }
}