
## Ean code validator

#### How to run

mvn clean test

See examples in code or tests

#### Description

Write program that validate ean-8 and ean-13 code.

Input:
* code as string
* type of ean (EAN-8, EAN-13)

Output: Valid code without addon

* Some products has addition code appended to ean code, so-called addon.
* Some scanners adds addon to ean code, for example, code ean code "6920702707721" can have addon "12" = "692070270772112") 
* Addons can be added to EAN-8 and also EAN-13
* Some scanners omits first "0", for example instead of "0075678164125", scanner return "075678164125"
* Add validation of input and exception handling
* Additional information: http://pl.wikipedia.org/wiki/EAN